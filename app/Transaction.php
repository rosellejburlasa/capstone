<?php

namespace App;

use Illuminate\Database\Eloquent\Model;


class Transaction extends Model
{
    protected $fillable = [
        'borrowed_date',
        'returned_date',
    ];

    protected $dates = ['borrowed_date', 'returned_date'];


    public function status()
    {
      return $this->belongsTo('App\Status', 'id');
    }

    public function item()
    {
      return $this->belongsTo('App\Item', 'id');
    }

}
