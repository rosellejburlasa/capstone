@extends('layouts.custom')
  
@section('content')
<div class="container pb-5 mb-5">	
	<div class="row">
		<div class="col-lg-6">
			<img class="img-fluid" src="{{ URL::to('/images/sale.jpg') }}" style="height:350px;">
		</div>
		<div class="col-lg-6">
			<div class="row">
				<div class="col-12">
					<img class="img-fluid mb-2 w-100" src="{{ URL::to('/images/homepage/pricing.JPG') }}">
				</div>
				<div class="col-12">
					<img class="img-fluid mb-2 w-100" src="{{ URL::to('/images/homepage/offer.JPG') }}">
				</div>
				<div class="col-12">
					<img class="img-fluid w-100" src="{{ URL::to('/images/homepage/CS.JPG') }}">
				</div>
			</div>
		</div>
	</div>
	<div class="row mt-2">
		{{--Filter--}}
		<div class="col-lg-6 mb-2">
			<label>Category: </label>
			<form action="/select" method="POST" class="d-flex">
	        	@csrf
				<select class="form-control" name="category_filter">
					<option value="0">All Categories</option>
					<option value="a-z">A-Z</option>
					<option value="z-a">Z-A</option>
					@foreach($categories as $category)
			    		<option value="{{ $category->id }}">
			    			{{ $category->name }}
			    		</option>
			    	@endforeach
				</select>
				<button type="submit" class="btn btn-warning mr-1">
					<i class="fas fa-filter"></i>
				</button>
			</form>
        </div>
        {{--Search--}}
        <div class="col-lg-4 mb-3 offset-lg-1">
	 		<div class="form-group">
		        <div class="form-group">
		        	<label>Search: </label>
		          	<form method="post" action="/search" class="form-group" role="search">
		          		@csrf
                        <div class="input-group mb-3">
                            <input type="search" class="form-control" name="search" id="txt_query" placeholder="Search Products">
                            <div class="input-group-append">
                                <button type="submit" class="btn btn-default input-group-text"><i class="fas fa-search"></i></button>
                            </div>
                        </div>
                    </form>
		        </div>
		    </div>
		</div>
	</div>
	<!-- Products -->
	<div class="row">
		@if(count($items)>0)
			@foreach($items as $item)
				<div class="col-md-4 mb-3 py-0">
	              	<div class="card">
	                 	<img src="{{$item->featured}}" class="card-img-top mh-50" id="item">
                    	<div class="card-body">
	                    	<h4><a href="{{ route('orders.show',$item->id) }}">{{$item->name}}</a></h4>
	                    	<h6>₱ {{$item->price}}</h6>
	                    	<h6>Current Stock: {{$item->stock}}</h6>
	                     	<a href="{{ route('orders.show',$item->id) }}" class="btn btn-block btn-primary mt-2">Read more...</a>
                    	</div>
	              	</div>
	            </div>
			@endforeach
		@else
	    	<div class="col col-lg-12 mb-3 d-inline-block">
	    		<div class="text-center mt-5">
	    			<h4>Sorry! Product is not available yet.</h4>
	    		</div>
	    	</div>
		@endif		
	</div>
</div>
@endsection