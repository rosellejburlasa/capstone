@extends('layouts.custom')

@section('content')
<div class="container my-5 py-5">
	@section('cart-rows')
		@php
			$total = 0;
			$shipping = 150;
		@endphp

		@foreach ($items as $item)
			@php
				$subtotal = $item->price * session('cart.'.$item->id) + $shipping;
				$total += $subtotal;
			@endphp
			<tr class="text-center">
				<td style="color:darkblue; font-weight:bold;">{{$item->name}}</td>
				<td><img src="{{$item->featured}}" class="img-thumbnail"  style="height:200px; width:200px;"></td>
				<td>{{number_format($item->price, 2)}}</td>
				<td>
					{{--Update Cart--}}
					<form method="POST" action="{{route('carts.update')}}">
						@csrf
						<input type="hidden" name="item_id" value="{{$item->id}}">
						<div class="btn-group btn-block">
							<input type="number" name="quantity" value="{{session('cart.'.$item->id)}}" class="form-control" required max="3">
							<button class="btn btn-success" type="submit">Update</button>
						</div>
					</form>
				</td>
				<td>{{number_format($subtotal, 2)}}</td>
				<td>
					{{--Delete--}}
					<form method="POST" action="{{ route('carts.remove')}}">
						@csrf
						<input type="hidden" name="item_id" value="{{$item->id}}">
						<button class="btn btn-danger p-3"><i class="fas fa-trash-alt fa-lg"></i></button>
					</form>
				</td>
			</tr>
		@endforeach
	@endsection
	<div class="container-fluid">
	@if(!empty(session('cart')))
		<header class="cart-header cf mb-2 display-4">
		    <strong>Items in Your Cart</strong>
		</header>
		@if ($errors->any())
			    <div class="alert alert-danger">
			        <strong>Whoops!</strong> There were some problems with your input.<br><br>
			        <ul>
			            @foreach ($errors->all() as $error)
			                <li>{{ $error }}</li>
			            @endforeach
			        </ul>
			    </div>
				@endif
		<table class="table table-bordered bg-light" style="color: black;">
			<thead class="text-center">
				<tr>
					<th>Item</th>
					<th>Image</th>
					<th>Price</th>
					<th>Quantity</th>
					<th>Subtotal</th>
					<th>Action</th>
				</tr>
			</thead>
			<tbody class="text-center">
				@yield('cart-rows')
				<tr class="text-center">
					<td></td>
					<td colspan="3" class="text-right pl-5"><b>Sub-Total</b></td>
					<td>{{number_format($total, 2)}}</td>
					<td></td>
				</tr>
				<tr class="text-center">
					<td></td>
					<td colspan="3" class="text-right pl-5"><b>Shipping Fee</b></td>
					<td>150.00</td>
					<td></td>
				</tr>
				<tr class="text-center">
					<td></td>
					<td colspan="3" class="text-right pl-5"><b>Grand Total</b></td>
					<td><strong>{{$total+150}}.00</strong></td>
					<td></td>
				</tr>
			</tbody>
		</table>

		{{--Empty Cart--}}
		<form action="{{route('carts.empty')}}" class="d-inline" method="get">
			@csrf
			<button class="btn btn-danger mb-5">Empty Cart</button>
		</form>
		<br>

		{{--Request for approval--}}
		<form action="/receipt" class="d-inline" method="post">
			@csrf
			<div class="mb-1">
				<label>Borrowed Date: </label>
        		<input type="date" name="borrowed_date" class="form-control mb-1 w-50" placeholder="Reserved Date">
        	</div>
        	<input type="hidden" name="totalPrice" value="{{ $total }}">
			<button class="btn btn-primary mt-2" type="submit">Request for approval</button>
		</form>
		<br>
		<button class="my-5"><a href="{{ route('orders.index') }}">Continue Shopping</a></button>
	@else
		<div class="container">
			<div class="row">
				<div class="col-lg-12 mb-3 text-center">
					<img class="img-fluid" src="{{ URL::to('/images/homepage/cart.JPG') }}" style="height: 350px;">
				</div>
				<div class="col-lg-12 text-center">
					<p>Check your <a href="{{route('transaction')}}">transaction history</a>.</p>
					<button class="btn text-center btn-outline-danger"><a href="{{ route('orders.index') }}">View Catalog</a></button>
				</div>
			</div>
		</div>
	@endif
	</div>
	<div class="copy-block mt-5 pt-5">    
	  	<p>Items will be saved in your cart for 30 days. To save items longer, add them to your <a href="#">Wish List</a>.</p>
	  	<p class="customer-care">
	    	Call us M-F 8:00 am to 6:00 pm EST<br />
	    	(877)-555-5555 or <a href="{{route('contact')}}">contact us</a>. <br />     
	  	</p>
	</div>    
</div>
@endsection