@extends('layouts.custom')

@section('content')
<div class="container my-5 py-5">
	<div class="row">
		{{--Categories--}}
	  	<div class="col-12">
	    	<div class="pull-right">
	      		<a class="btn btn-outline-primary mb-3" href="{{ route('categories.create') }}"> Create New Category</a>
	    	</div>
	    	@if(count($categories)>0)
			<table class="table table-hover text-center">
				<thead class="table-primary">
					<th>
						Category Name
					</th>
					<th>
						Actions
					</th>
				</thead>
				<tbody>
					@foreach($categories as $category)
						<tr class="table-secondary">
							<td><strong>{{$category->name}}</strong></td>
							<td>
								<form method="POST" action="{{ route('categories.destroy',$category->id) }}">
									<a type="button" href="{{ route('categories.edit',$category->id) }}" class="btn btn-success">Update</a>
									@csrf
									@method('DELETE')
									<button class="btn btn-danger">Remove</button>
								</form>
							</td>
						</tr>
					@endforeach
				</tbody>
			</table>
			@else
			  <p>No Inserted Category!</p>
			@endif
			  {!! $categories->links() !!}  
	  	</div>
	</div>
</div>
@endsection