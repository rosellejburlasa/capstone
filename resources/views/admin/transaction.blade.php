@extends('layouts.custom')

@section('content')
<div class="container my-5 py-5">
    <div class="row">
        <div class="col-12">
            <h2 class="text-center">Transaction History</h2>
          <table id="example" class="table table-striped table-hover dt-responsive nowrap" id="dataTable">
            <thead class="table-primary">
              <tr>
                <th>Products</th>
                <th>Quantity</th>
                <th>Borrowed Date</th>
                <th>Returned Date</th>
                <th>Status</th>
                <th>Action</th>
              </tr>
            </thead>
            <tbody>
              @foreach($transactions as $transaction)
                <tr class="table-secondary">
                    <td><strong>{{$transaction->item->name}}</strong></td>
                    <td>{{$transaction->qty_rented}}</td>
                    <td>{{$transaction->borrowed_date}}</td>
                    <td>{{$transaction->returned_date}}</td>
                    <td>
                        @if($transaction->status_id == 1)
                            Pending
                        @elseif($transaction->status_id == 2)
                            Cancelled
                        @elseif($transaction->status_id == 3)
                            Approved
                        @elseif($transaction->status_id == 4)
                            Rejected
                        @else
                            Returned
                        @endif
                    </td>
                    @if($transaction->status_id == 1)
                    <td>
                    	<form method="POST" action="{{ route('view.approved',['id'=>$transaction->id]) }}">
                            @csrf
                            <input type="hidden" name="status_id" value="3">
                            <button class="btn btn-primary" type="submit">Approved</button>
                        </form>
                        <br>
                        <form method="POST" action="{{ route('view.rejected',['id'=>$transaction->id]) }}">
                            @csrf
                            <input type="hidden" name="status_id" value="4">
                            <button class="btn btn-danger" type="submit">Rejected</button>
                        </form>
                    </td>
                    @else
                        <td> No action needed!</td>
                    @endif
                </tr>
              @endforeach
            </tbody>
          </table>
        </div>
    </div>
</div>
@endsection
