@extends('layouts.custom')

@section('content')
<div class="container my-5 pt-5">
	<div class="row">
		<div class="col-lg-8 offset-lg-2">
			@if(count($errors) > 0)
				<ul class="list-group">
					@foreach($errors->all() as $error)
						<li class="list-group-item text-danger">
							{{$error}}
						</li>
					@endforeach
				</ul>
			@endif
			<br>
			<div class="card">
				<div class="card-header">
					Update Product: <strong class="display-5">{{$item->name}}</strong>
				</div>
				<div class="card-body">
					{{-- Form for update the product --}}
					<div class="card-body">
						<form method="post" action="{{ route('products.update',$item->id) }}" enctype="multipart/form-data">
							@csrf
							@method('PUT')
							<div class="form-group">
								<label>Name</label>
								<input type="text" name="name" class="form-control" value="{{$item->name}}">
							</div>
							<div class="form-group">
								<label>Serial</label>
								<input type="text" name="serial" class="form-control" value="{{$item->serial}}">
							</div>
							<div class="form-group">
								<label>Stock</label>
								<input type="number" name="stock" class="form-control">
							</div>
							<div class="form-group">
								<label>Price</label>
								<input type="number" name="price" class="form-control" value="{{$item->price}}">
							</div>
							<div class="form-group">
								<label>Allowed Age</label>
								<input type="text" name="age" class="form-control" value="{{$item->age}}">
							</div>
							<div class="form-group">
								<label>Images</label>
								<input type="file" name="featured" class="form-control p-1" >
							</div>
							<div class="form-group">
								<label>Includes</label>
								<textarea class="form-control" name="includes" cols="5" rows="6" value="{{$item->includes}}"></textarea>
							</div>
							<div class="form-group">
								<label>Number of players allowed</label>
								<input type="text" name="player" class="form-control" value="{{$item->player}}">
							</div>
							<div class="form-group">
								<label>Description</label>
								<textarea class="form-control" name="description" cols="5" rows="6" value="{{$item->description}}"></textarea>
							</div>
							<div class="form-group">
								<label>Category:</label>
								<select class="form-control" name="category_id">
									<option disabled selected>Select the Category</option>
									@foreach($categories as $category)
										<option value="{{$category->id}}">{{$category->name}}</option>
									@endforeach
								</select>
							</div>
							<div class="form-group">
								<button class="btn btn-primary" type="submit">Submit Products</button>
							</div>	
						</form>
						<a class="btn btn-outline-primary" href="{{ route('products.index') }}"> Back</a>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
@endsection