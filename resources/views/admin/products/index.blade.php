@extends('layouts.custom')

@section('content')
<div class="container my-5 py-5">
	<div class="row">		
		{{--Products--}}
	  	<div class="col-12">
	    	<div class="pull-right">
	      		<a class="btn btn-outline-primary mb-3" href="{{ route('products.create') }}"> Create New Product</a>
	    	</div>
	    	<div class="col-lg-4 offset-lg-8 mb-2">
				<form action="/select" method="POST" class="d-flex">
		        	@csrf
					<select class="form-control" name="category_filter">
						<option value="0">All Categories</option>
						<option value="a-z">A-Z</option>
						<option value="z-a">Z-A</option>
						@foreach($categories as $category)
				    		<option value="{{ $category->id }}">
				    			{{ $category->name }}
				    		</option>
				    	@endforeach
					</select>
					<button type="submit" class="btn btn-warning mr-1">
						<i class="fas fa-filter"></i>
					</button>
				</form>
	        </div>
			<table id="example" class="table table-striped table-hover dt-responsive nowrap" id="dataTable">
				<thead class="table-primary">
					<tr>
						<th>Product Name</th>
						<th>Serial#</th>
						<th>Age</th>
						<th>No. of Player</th>
						<th>Price</th>
						<th>Stock</th>
						<th>Action</th>
					</tr>
				</thead>
				<tbody>
					@foreach($items as $item)
						<tr class="table-secondary">
							<td><strong>{{$item->name}}</strong></td>
							<td>{{$item->serial}}</td>
							<td>{{$item->age}}</td>
							<td>{{$item->player}}</td>
							<td><strong>{{$item->price}}</strong></td>
							<td>{{$item->stock}}</td>
							<td>
								<form method="POST" action="{{ route('products.destroy',$item->id) }}">
									@csrf
									<a href="{{ route('products.show',$item->id) }}" class="btn btn-primary"><i class="fas fa-eye"></i></a>
									<a type="button" href="{{ route('products.edit',$item->id) }}" class="btn btn-success"><i class="fas fa-edit"></i></a>
									@method('DELETE')
									<button class="btn btn-danger"><i class="fas fa-trash-alt"></i></button>
								</form>
							</td>
						</tr>
					@endforeach
				</tbody>
			</table>
			 {!! $items->links() !!}
	  	</div>
	</div>
</div>
@endsection